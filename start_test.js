const config = require("./config.json");
const {
  createInsecure,
  structProtoToJson
} = require("@skitter/message-service-grpc");
const timediff = require("timediff");
const Spinnies = require("spinnies");
const chalk = require("chalk");

const { table } = require("table");
const csvStringify = require("csv-stringify");
const fs = require("fs-extra");

const INPUT = config.input;
const WS = config.workspace;
const CONTEXT = config.context_variables_to_added;
const NUMBER_OF_ITERATIONS = config.number_of_iterations;

const instance = createInsecure(config.grpcAddress);

const calls = [];

const reportPath =
  "./reports/" +
  Date.now() +
  "_message_service_with_" +
  NUMBER_OF_ITERATIONS +
  ".csv";

function callSendMessage() {
  return new Promise((resolve, reject) => {
    const call = instance.sendMessage({
      input: INPUT,
      workspace: WS,
      context: CONTEXT
    });
    call.on("error", error => {
      if (error.code !== 2) {
        reject(error);
      }
    });

    call.on("data", data => {
      resolve(data);
    });
  });
}

const allCallPromises = [];

const spinner = {
  interval: 60,
  frames: [
    "🕐",
    "🕑",
    "🕒",
    "🕓",
    "🕔",
    "🕕",
    "🕖",
    "🕖",
    "🕘",
    "🕙",
    "🕚",
    "🕛"
  ]
};
const spinnies = new Spinnies({
  color: "blue",
  spinner
});

const timeToText = time =>
  `${time.minutes} minutes, ${time.seconds} seconds, ${time.milliseconds} ms`;

for (let index = 0; index < NUMBER_OF_ITERATIONS; index++) {
  const conversationName = "#conversation " + (index + 1);
  const callObj = {
    name: conversationName,
    start_time: Date.now(),
    end_time: null,
    text: null,
    status: null,
    timeElapsed: null,
    failed_reason: null
  };
  calls.push(callObj);
  spinnies.add(conversationName, { text: "Sending " + conversationName });

  const promise = callSendMessage().then(
    data => {
      callObj.text = data.output.text.join("\n");
      callObj.status = "passed";
      callObj.end_time = Date.now();
      callObj.timeElapsed = timediff(
        callObj.start_time,
        callObj.end_time,
        "mSs"
      );
      spinnies.succeed(conversationName, {
        text:
          conversationName +
          " is success within " +
          timeToText(callObj.timeElapsed)
      });
    },
    err => {
      callObj.status = "failed";
      callObj.failed_reason = JSON.stringify(err);
      callObj.end_time = Date.now();
      callObj.timeElapsed = timediff(
        callObj.start_time,
        callObj.end_time,
        "mSs"
      );
      spinnies.fail(conversationName, {
        text:
          conversationName +
          " is failed within " +
          timeToText(callObj.timeElapsed)
      });
    }
  );
  allCallPromises.push(promise);
}

const toRow = call => [
  call.name,
  (call.text.length && call.text) || "''",
  call.status,
  timeToText(call.timeElapsed),
  (call.failed_reason && call.failed_reason.length && call.failed_reason) || "-"
];

const createReport = calls => {
  const title = [
    "conversation",
    "output text",
    "status",
    "time elapsed",
    "reason if failed"
  ];
  return [title, ...calls.map(toRow)];
};

const chalkStatus = ([name, text, status, ...rest]) => [
  name,
  text,
  status == "passed" ? chalk.green("passed") : chalk.bold.red("failed"),
  ...rest
];

const createTable = report => {
  const [title, ...rows] = report;
  return table([title.map(t => chalk.blue(t)), ...rows.map(chalkStatus)]);
};

Promise.all(allCallPromises).then(() => {
  const report = createReport(calls);

  csvStringify(report, (err, output) => {
    if (err) {
      console.log("error ::: ", err);
    } else {
      fs.writeFile(reportPath, output).then(
        () => {
          console.log(createTable(report));
        },
        err => {
          console.log("csv  failed error ::: ", err);
        }
      );
    }
  });
});
